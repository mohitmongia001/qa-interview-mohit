Feature: An Example Feature

  Scenario: An example scenario
    Given   I am on the homepage
    Then    I validated all the input fields

  Scenario: A negative scenario - First Name is Mandatory
    Given   I am on the homepage
    And   I click on "Suggest Male Rap Name" button
    Then   I see the text "You must enter your first name."

  Scenario Outline: Submit and Validate Male name list
    Given  I am on the homepage
    When   I enters "First Name" as <name>
    Then   I "checked" the Use Nickname checkbox
    And   I click on "Suggest Male Rap Name" button
    Then   I validated the name list

    Examples:
    |name|
    |John |


  Scenario Outline: Submit and Validate Female name list
    When   I enters "First Name" as <name>
    Then   I enters "Last Initial" as <lastInitials>
    Then   I "unchecked" the Use Nickname checkbox
    And    I click on "Suggest Female Rap Name" button
    Then   I validated the name list

    Examples:
      |name | lastInitials |
      |Tina | G            |
      |Gina | T            |


