Given(/^I am on the homepage$/) do
  find('h1', :text => "Welcome to MyRapName.com")
end

=begin
Then(/^I see the text "(.*?)"$/) do |text|
  page.has_text?(text)
end
=end

Then(/^I validated all the input fields$/) do
=begin
  find(:xpath, "//input")
=end
  puts "Below are the input fields:"
  page.all(:xpath, "//input").each do |val|
    att = val['name']
    type = val['type']
    puts att + " - " +type + " field"
  end

end


And(/^I click on "(.*?)" button$/) do |field|

  if field.eql? "Suggest Male Rap Name"
    click_button('Male')
  else
    click_button('Female')
  end

end

Then(/^I see the text "(.*?)"$/) do |text|

  find('h1', :text => text)
end

When(/^I enters "(.*?)" as (.*?)$/) do |field, input|

  if field.eql? "First Name"
    find(:xpath, "//input[@name='firstname']").set(input)

  elsif field.eql? "Last Initial"
         find(:xpath, "//input[@name ='lastinitial']").set(input)
  end

end

Then(/^I validated the name list$/) do
  listCount =  page.all(:xpath, "(//table)[4]/tbody/tr").length

  (1..listCount).each do |i|
    puts find(:xpath, "(//table)[4]/tbody/tr["+ i.to_s + "]/td/div/h1").text
  end

end


Then(/^I "(.*?)" the Use Nickname checkbox$/) do |text|
  if text.eql? "unchecked"
    if find(:xpath,"//input[@name ='shorten' and @type = 'checkbox']").checked?
      find(:xpath,"//input[@name ='shorten' and @type = 'checkbox']").set(false)
    end
  end

    #sleep 5
end



