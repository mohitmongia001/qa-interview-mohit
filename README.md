# QA Code Test

### Setup
1. Clone this repo (if using git)
2. Create a new branch OR Rename your project "qa-interview-yourname"
3. Make sure your Ruby environment is using `ruby 2.3.3`.
4. Run the example feature file.  Make sure it runs correctly as you will build upon this test.
    - `bundle exec cucumber features/example.feature`
5. Create the tests below in order.  You will create tests by building on the files that are already set up in this project.

Homepage='http://www.myrapname.com/'

### Tests
Write an automated test to validate each of the following:
1. The input fields exist
2. A negative scenario using the input fields
3. Input data into the fields
4. Submit a name for male with a nickname and validate that a new name has been prepended to the list
5. Submit twice for a female with a last initial and validate that a new name has been prepended to the list

### Solutions
1. The input fields exist - Created scenario to validate and Print the Input fields - This will validate the 5 input fields on the page
2. A negative scenario using the input fields - Created Scenario for empty First Name field and validating the error message on the page
3. Input data into the fields - Merged scenario 3 with scenario 4 and 5
4. Submit a name for male with a nickname and validate that a new name has been prepended to the list - Created scenario to enter first name and printing the generated name in console. Used cucumber examples to pass the First name
5. Submit twice for a female with a last initial and validate that a new name has been prepended to the list - Created scenario with two cucumber examples and printing the generated names in console


